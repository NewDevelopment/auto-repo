package com.Way2Assessment;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;



public class PublicAPI {
	
	/*produce a list of all dog breeds.*/
	@Test
	public void GetAllDogBreeds() {
		
		RestAssured.baseURI="https://dog.ceo/api/breeds/list/all";
		RequestSpecification httpRequest  = RestAssured.given();
		Response resp = httpRequest.request(Method.GET);
		
		int statusCode = resp.getStatusCode();
		System.out.println("Status code:"+statusCode);
		
		String responseBody = resp.getBody().asString();
		System.out.println("The response is:=>" +responseBody );
		
		Assert.assertEquals(statusCode, 200);
	}

	/*verify “retriever” breed is within the list.*/
	@Test
	public void VerifyRetrieverBreed() {
		RestAssured.baseURI="https://dog.ceo/api/breeds/list/all";
		RequestSpecification httpRequest  = RestAssured.given();
		Response resp = httpRequest.request(Method.GET);
		
		ResponseBody body = resp.getBody();
		String bodyAsString = body.asString();
		Assert.assertEquals(bodyAsString.toLowerCase().contains("retriever"), true);
		
	}
	
	/*produce list of sub-breeds for “retriever”*/
	@Test
	public void GetListOfSubBreedsForRetriever() {
		
		RestAssured.baseURI="https://dog.ceo/api/breed/retriever/list";
		RequestSpecification httpRequest  = RestAssured.given();
		Response resp = httpRequest.request(Method.GET);
		
		int statusCode = resp.getStatusCode();
		System.out.println("Status code:"+statusCode);
		
		String responseBody = resp.getBody().asString();
		System.out.println("The response is:=>" +responseBody );
		
		Assert.assertEquals(statusCode, 200);
	}
	
	/*produce a random image / link for the sub-breed “golden”*/
	
	@Test
	public void GetRondomImageForGolden() {
		
		RestAssured.baseURI="https://dog.ceo/api/breed/retriever/golden/images/random";
		RequestSpecification httpRequest  = RestAssured.given();
		
		Response resp = httpRequest.request(Method.GET);
		int statusCode = resp.getStatusCode();
		System.out.println("Status code:"+statusCode);
		
		String responseBody = resp.getBody().asString();
		System.out.println("The response is:=>" +responseBody );
		
		Assert.assertEquals(statusCode, 200);
		
	}
}
