package com.Way2Assessment;

import java.util.concurrent.TimeUnit;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class WebTestCases {

	WebDriver driver = null;

	/* Before test we open the browser */
	@BeforeTest
	public void setUp() {
		System.setProperty("webdriver.gecko.driver",
				"C:\\Users\\Hp 6560b\\Desktop\\Assessment-Framework\\AutomationAssessmentCore\\FrameworkDrivers\\geckodriver.exe");
		this.driver = new FirefoxDriver();
		String baseUrl = "http://www.way2automation.com/angularjs-protractor/webtables/";
		this.driver.manage().window().maximize();
		this.driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		this.driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
		this.driver.get(baseUrl);

	}

	/* After test we close the browser */
	@AfterTest
	public void tearDown() {
		this.driver.close();

	}

	@DataProvider(name = "testdata1")

	public Object[][] TestDataProvider() {
		return new Object[][] {

				new Object[] { "Onke" }, };
	}

	@Test(dataProvider = "testdata1")
	public void validateUserOnTheList(String Name) {
		try {

			driver.findElement(By.xpath("/html/body/table/thead/tr[1]/td/input")).sendKeys(Name);
			
			WebElement tableColumn = driver.findElement(By.xpath("/html/body/table/tfoot/tr/td"));
			Assert.assertEquals(true, tableColumn.isDisplayed());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@DataProvider(name = "testdata2")

	public Object[][] TestDataFeed() {
		return new Object[][] {

				new Object[] { "FNme1", "LName1", "User1", "Pass1", "admin@gmail.com",
						"082555" },
				new Object[] { "FNme2", "LName2", "User2", "Pass2", "customer@gmail.com",
						"083444" },

		};

	}

	@SuppressWarnings("unchecked")
	@Test(dataProvider="testdata2")
	public void addNewUser(String firstName, String lastName, String username, String password,  String email, String tellPhone) {
		
		driver.findElement(By.className("btn btn-link pull-right")).click();
		driver.findElement(By.name("FirstName")).sendKeys(firstName);
		driver.findElement(By.name("LastName")).sendKeys(lastName);
		driver.findElement(By.name("UserName")).sendKeys(username);
		driver.findElement(By.name("UserName")).sendKeys(password);
		List<WebElement> customer = (List<WebElement>) driver.findElement(By.name("optionsRadios"));
		boolean bValue = false;
		
		 bValue = customer.get(0).isSelected();
		 if(bValue == true){
			 customer.get(1).click();
			 }else{
				 customer.get(0).click();
			 }
	
		 WebElement role = (WebElement) new Select(driver.findElement(By.id("RoleId")));
		boolean roleValue = false;
		 if(roleValue == true){
				Select userRole=new Select(role);
				userRole.selectByIndex(3);
			 }else{
				 Select userRole=new Select(role);
					userRole.selectByIndex(2);
			 }
		
		driver.findElement(By.name("Email")).sendKeys(email);
		driver.findElement(By.name("Mobilephone")).sendKeys(tellPhone);
		
		driver.findElement(By.className("btn btn-success")).click();
		

	}
	
	
	@DataProvider(name = "testdata3")

	public Object[][] CheckAddedUser() {
		return new Object[][] {

				new Object[] { "FName1" }, 
				new Object[] { "FName2" }, 
				};
	}
	@Test(dataProvider = "testdata3")
	public void verifyAddedUser(String addedName){
		try {

			driver.findElement(By.xpath("/html/body/table/thead/tr[1]/td/input")).sendKeys(addedName);
			
			WebElement tableColumn = driver.findElement(By.xpath("/html/body/table/tfoot/tr/td"));
			Assert.assertEquals(true, tableColumn.isDisplayed());

		} catch (Exception e) {
			e.printStackTrace();
		}

		
		
		
	}

}
